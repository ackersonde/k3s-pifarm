variables:
  KUBE_CONTEXT: ackersonde/k3s-pifarm:tuxedolinuxrex # k8s agent (see .gitlab/agents/tuxedolinuxrex/config.yaml)
  DEPLOY_IMG: alpine:latest
  GO_BUILD_IMG: golang:1.22-alpine
  DEPLOYMENT_ENV: docker
  RUNNER_SERVER: deploy@docker
  VAULT_ADDR: $VAULT_SERVER_URL
  VAULT_SERVER_URL: $VAULT_SERVER_URL
  VAULT_TOKEN: unset
  HETZNER_API_TOKEN: unset
  SLACK_NOTIFY_TOKEN: unset
  GIT_STRATEGY: clone

.base:
  tags:
    - deploy@docker
  # See https://gitlab.com/gitlab-org/cluster-integration/cluster-applications/
  image: "registry.gitlab.com/gitlab-org/cluster-integration/cluster-applications:v2.5.0"
  id_tokens:
    VAULT_ID_TOKEN:
      aud: https://gitlab.com
  before_script: | # Since gitlab doesn't double extend, we can't reuse .vault_token :/ and have to copy it here
    ssh-add ~/.ssh/id_ed25519 || eval $(ssh-agent) && ssh-add ~/.ssh/id_ed25519

    VAULT_TOKEN=$(vault write -field=token auth/jwt/login role=gitlab jwt=$VAULT_ID_TOKEN)
    HETZNER_API_TOKEN=$(vault kv get -field=api_arm64_cloud_token github/ackersonde/hetzner)

    if [[ -f $KUBECONFIG ]] && command -v kubectl; then
      chmod 600 $KUBECONFIG
      echo "using context $KUBE_CONTEXT"
      gl-use-kube-context
    fi

stages:
  - init
  - diff
  - sync

init: #ialize a k3s cluster w/ helm & gitlab support
  stage: init
  extends: .base
  script: |
    SLACK_NOTIFY_TOKEN=$(vault kv get -field=bender_bot_api_token github/ackersonde/slack)
    DISCORD_NOTIFY_TOKEN=$(vault kv get -field=DEPLOY_WEBHOOK github/ackersonde/discord)

    echo "K3S_TOKEN=$(vault kv get -field=server_token github/ackersonde/k3s)" >> build_env
    echo "TARGET_HOST=$TARGET_HOST" >> build_env
    echo "CI_PIPELINE_URL=$CI_PIPELINE_URL" >> build_env
    echo "CI_PROJECT_NAMESPACE=$CI_PROJECT_NAMESPACE" >> build_env
    echo "CI_PROJECT_NAME=$CI_PROJECT_NAME" >> build_env
    echo "CI_PIPELINE_IID=$CI_PIPELINE_IID" >> build_env
    echo "SLACK_NOTIFY_TOKEN=$SLACK_NOTIFY_TOKEN" >> build_env
    echo "DISCORD_NOTIFY_TOKEN=$DISCORD_NOTIFY_TOKEN" >> build_env
    scp build_env ackersond@$TARGET_HOST:/tmp/

    ssh ackersond@$TARGET_HOST -- '
    set -o allexport; source /tmp/build_env; set +o allexport;
    if [[ ! -f ~/.kube/config ]] && [[ ! -f /usr/local/bin/kubectl ]] || ! kubectl get --raw="/readyz" | grep -q ok; then
      echo "Cluster not running and/or no KUBECONFIG file found; (Re)installing k3s......"

      export K3S_KUBECONFIG_MODE="644"
      export NODE_TYPE="server --disable=traefik"
      export CIDRS="--cluster-cidr=10.42.0.0/16,2001:cafe:42::/56 --service-cidr=10.43.0.0/16,2001:cafe:42::/112"
      # export EXTRAS="--kubelet-arg=allowed-unsafe-sysctls=net.core.rmem_max,net.core.wmem_max" # needed for syncthing UDP buffers

      if [[ $TARGET_HOST != "tuxedolinuxrex.ackerson.de" ]]; then
        export NODE_TYPE="agent --server https://192.168.178.69:6443" # internal IP of the server node
        export CIDRS="" # Agents do NOT need to know about the cluster CIDRs
      else
        echo "Install NFS server for persistent volumes in the cluster..."
        sudo apt-get install nfs-kernel-server --yes
        echo "/srv/k3s        192.168.178.0/24(rw,sync,fsid=0,crossmnt,no_subtree_check)" | sudo tee /etc/exports
        sudo exportfs -a && sudo systemctl restart nfs-server.service
      fi

      export INSTALL_K3S_EXEC="$NODE_TYPE --token $K3S_TOKEN $CIDRS"
      echo "Installing k3s on '$TARGET_HOST' as $NODE_TYPE..."
      curl -sfL https://get.k3s.io | sh - || true

      if [[ $TARGET_HOST == "tuxedolinuxrex.ackerson.de" ]]; then
        export KUBECONFIG=~/.kube/config
        mkdir ~/.kube 2> /dev/null
        sudo k3s kubectl config view --raw > "$KUBECONFIG"
        chmod 600 "$KUBECONFIG"
        echo "KUBECONFIG=~/.kube/config" >> ~/.bashrc

        kubectl get --raw="/readyz" | grep -q ok || exit 1

        # setup gitlab registry, docker deployment creds to pull new application images
        kubectl create secret docker-registry gitlab-registry-credentials \
          --namespace gitlab-managed-apps \
          --docker-server=registry.gitlab.com \
          --docker-username=$(vault kv get -field=username github/ackersonde/gitlab) \
          --docker-password=$(vault kv get -field=registry_read_token github/ackersonde/gitlab)

        kubectl create secret docker-registry docker-hub-secret \
          --namespace gitlab-managed-apps \
          --docker-server="https://docker.io" \
          --docker-username=$(vault kv get -field=username github/ackersonde/docker) \
          --docker-password=$(vault kv get -field=token github/ackersonde/docker)
        echo "Connect the cluster agent w/ Gitlab @ https://docs.gitlab.com/ee/user/clusters/management_project_template.html"
      fi

      curl -s \
      --data content="Installed k3s on $TARGET_HOST: [$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME @ $CI_PIPELINE_IID]($CI_PIPELINE_URL)" \
      --request POST $DEPLOY_WEBHOOK

      curl -s -o /dev/null -X POST -d token=$SLACK_NOTIFY_TOKEN -d channel=C092UE0H4 \
        -d text=" <$CI_PIPELINE_URL|$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME @ $CI_PIPELINE_IID>" \
        https://slack.com/api/chat.postMessage
    else
      curl -s \
      --data content="Cowardly refusing to reinstall k3s - remove by:
    \`ackersond@$TARGET_HOST:~ $ k3s-(agent)-uninstall.sh; rm -Rf ~/.kube/\`
    and rerun <$CI_PIPELINE_URL|this pipeline> if you feel brave!" --request POST $DEPLOY_WEBHOOK

      curl -s -o /dev/null -X POST -d token=$SLACK_NOTIFY_TOKEN -d channel=C092UE0H4 \
        -d text="Cowardly refusing to reinstall k3s - remove by:
    \`ackersond@$TARGET_HOST:~ $ k3s-(agent)-uninstall.sh; rm -Rf ~/.kube/\`
    and rerun <$CI_PIPELINE_URL|this pipeline> if you feel brave!" https://slack.com/api/chat.postMessage
    fi

    # Helm & helmfile + vals setup (vals is used by helmfile to decrypt Vault secrets :)
    if [[ $TARGET_HOST == "tuxedolinuxrex.ackerson.de" ]] && ([[ ! -f /usr/sbin/helm ]] || [[ ! -f /usr/local/bin/helmfile ]]); then
      curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
      sudo apt-get install apt-transport-https --yes
      echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
      sudo apt-get update --yes
      sudo apt-get install helm --yes
      helm version

      ARCH=arm64
      if [[ $TARGET_HOST == "tuxedolinuxrex.ackerson.de" ]]; then
        ARCH=amd64
      fi
      curl -o /tmp/helmfile.tgz -L https://github.com/helmfile/helmfile/releases/download/v1.0.0-rc.2/helmfile_1.0.0-rc.2_linux_$ARCH.tar.gz
      tar zxf /tmp/helmfile.tgz -C /tmp
      sudo mv /tmp/helmfile /usr/local/bin/helmfile
      sudo chmod +x /usr/local/bin/helmfile

      curl -o /tmp/vals.tgz -L https://github.com/helmfile/vals/releases/download/v0.37.3/vals_0.37.3_linux_$ARCH.tar.gz
      tar zxf /tmp/vals.tgz -C /tmp
      sudo mv /tmp/vals /usr/local/bin/vals
      sudo chmod +x /usr/local/bin/vals

      helm plugin install https://github.com/databus23/helm-diff || true
      helm plugin install https://github.com/jkroepke/helm-secrets || true
      helm plugin install https://github.com/hypnoglow/helm-s3.git || true
      helm plugin install https://github.com/aslafy-z/helm-git --version 1.3.0 || true
      helmfile init

      # Gitlab cluster helper scripts
      sudo curl -o /usr/local/bin/gl-ensure-namespace "https://gitlab.com/gitlab-org/cluster-integration/cluster-applications/-/raw/master/src/bin/gl-ensure-namespace?ref_type=heads&inline=false"
      sudo curl -o /usr/local/bin/gl-use-kube-context "https://gitlab.com/gitlab-org/cluster-integration/cluster-applications/-/raw/master/src/bin/gl-use-kube-context?ref_type=heads&inline=false"
      sudo curl -o /usr/local/bin/gl-helmfile "https://gitlab.com/gitlab-org/cluster-integration/cluster-applications/-/raw/master/src/bin/gl-helmfile?ref_type=heads&inline=false"
      sudo chmod +x /usr/local/bin/gl-ensure-namespace /usr/local/bin/gl-use-kube-context /usr/local/bin/gl-helmfile

      # Cert manager + webhook-hetzner TLS setup
      helm repo add jetstack https://charts.jetstack.io --force-update
      helm repo add cert-manager-webhook-hetzner https://vadimkim.github.io/cert-manager-webhook-hetzner
      helm repo update

      # cert-manager install w/ hetzner webhook
      # `installCRDS=true` MUST be set (crds.enabled=true does NOT work!)
      helm install \
        cert-manager jetstack/cert-manager \
        --namespace cert-manager \
        --create-namespace \
        --version v1.15.1 \
        --set installCRDs=true

      helm install \
        cert-manager-webhook-hetzner cert-manager-webhook-hetzner/cert-manager-webhook-hetzner \
        --namespace cert-manager \
        --version v1.3.1 \
        --set groupName=acme.ackerson.de
    fi

    rm /tmp/build_env'

diff:
  stage: diff
  extends: .base
  environment:
    name: production
    action: prepare
  script: |
      gl-helmfile init
      gl-helmfile --file $CI_PROJECT_DIR/helmfile.yaml diff --suppress-secrets
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

sync:
  stage: sync
  extends: .base
  environment:
    name: production
  script: |
      gl-ensure-namespace gitlab-managed-apps
      gl-helmfile --file $CI_PROJECT_DIR/helmfile.yaml sync
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
