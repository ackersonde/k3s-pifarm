# k3s-pifarm

[![pipeline status](https://gitlab.com/ackersonde/k3s-pifarm/badges/master/pipeline.svg)](https://gitlab.com/ackersonde/k3s-pifarm/-/commits/master)

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/user/clusters/management_project_template.html).
## Upgrading the cluster

For [Automated Upgrades](https://docs.k3s.io/upgrades/automated):

### Add `system-upgrade-controller`:

```
kubectl create namespace system-upgrade
kubectl apply -f https://github.com/rancher/system-upgrade-controller/releases/latest/download/crd.yaml
kubectl apply -f https://github.com/rancher/system-upgrade-controller/releases/latest/download/system-upgrade-controller.yaml
```

### Prepare upgrade plans

```
# Agent plan
apiVersion: upgrade.cattle.io/v1
kind: Plan
metadata:
  name: agent-plan
  namespace: system-upgrade
spec:
  concurrency: 1
  cordon: true
  nodeSelector:
    matchExpressions:
    - key: node-role.kubernetes.io/control-plane
      operator: DoesNotExist
  prepare:
    args:
    - prepare
    - server-plan
    image: rancher/k3s-upgrade
  serviceAccountName: system-upgrade
  upgrade:
    image: rancher/k3s-upgrade
  channel: https://update.k3s.io/v1-release/channels/stable

---
# Server plan
apiVersion: upgrade.cattle.io/v1
kind: Plan
metadata:
  name: server-plan
  namespace: system-upgrade
spec:
  concurrency: 1
  cordon: true
  nodeSelector:
    matchExpressions:
    - key: node-role.kubernetes.io/control-plane
      operator: In
      values:
      - "true"
  serviceAccountName: system-upgrade
  upgrade:
    image: rancher/k3s-upgrade
  channel: https://update.k3s.io/v1-release/channels/stable
```
To constantly keep up-to-date, replace `version: XYZ` w/ `channel: https://update.k3s.io/v1-release/channels/stable` and the kluster will keep all nodes on the latest stable version.

### Manual Upgrades

Alternatively, you may upgrade the server (or agents) by [manually reinstalling k3s](https://docs.k3s.io/upgrades/manual) via the **same** config options as found in `./gitlab-ci.yml`:

```
export K3S_TOKEN=<vault>
curl -sfL https://get.k3s.io | sh -s - server --token $K3S_TOKEN --disable=traefik --cluster-cidr=10.42.0.0/16,2001:cafe:42::/56 --service-cidr=10.43.0.0/16,2001:cafe:42::/112
```

Note the agent install script will **NOT** look like the server one above ;)

### Upgrading the local gitlab-runner

Don't get confused by the gitlab-runner running **inside** the k3s cluster [here](./applications/gitlab-runner/helmfile.yaml#L8) -> this will basically keep upgrading this to the latest version forever!

As this repo is intended to **create** the k8s cluster, it has a local, manually installed `gitlab-runner` via `cakefor:~ $ sudo curl -L --output /usr/local/bin/gitlab-runner "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/binaries/gitlab-runner-linux-arm64"`.

To upgrade, you'll have to first `sudo gitlab-runner stop`, run the above `curl` fetching the latest version & then `sudo gitlab-runner start`.

## Local k3s cluster

This repo is responsible for installing & maintaining the k3s cluster in my local pi farm.

Cluster nodes must include `cgroup_memory=1 cgroup_enable=memory` in the boot file `/boot/firmware/cmdline.txt` (requires a reboot).

By default, it will install and work on the main, control-plane k3s node `tuxedolinuxrex`. This is also where the helm charts will be applied as the main node then communicates with any connected worker nodes accordingly.

Supplying an env variable `TARGET_HOST=thor` will add that host to the cluster as a worker node.

- Helmfile order (both in top file as well as individual releases:) is critical!
- To remove charts, do set `installed: false` where appropriate and deploy. Manually deleting causes drift, errors and suffering :/

### Upgrading helm applications

Version control is important! You'll find the helm charts on github (e.g. [Traefik](https://github.com/traefik/traefik-helm-chart/tree/master) & [Vault](https://github.com/hashicorp/vault-helm)).

Within the `applications/<app-name>/helmfile.yaml` there's a `releases/version:` field: update appropriately in order to update the application version! (see helm repo update above)

Long story short: keep these versions updated and then follow the `scale`in & `scale`out [instructions here for upgrades](https://gitlab.com/ackersonde/vault/-/commit/3902594256e20f03bf0b514c96a3e6260d456716).

#### Upgrade Gitlab Runner via `helm`

The helm chart is automatically pinned to latest so it will update in the background. However, the gitlab-runner deployment **will not** be upgraded! As Gitlab frequently deprecates/upgrades these runners, follow these steps regularly (once a quarter/twice a year).

Manually replace these variables at the top of the [template](./applications/gitlab-runner/values.yaml.gotmpl):

```
gitlabUrl: {{ requiredEnv "CI_SERVER_URL" | quote }} # this is https://gitlab.com
runnerToken: ref+vault://github/ackersonde/gitlab#/k8s-runner_auth_token
```

Then simply execute:

```
$ helm upgrade -n gitlab-managed-apps -f applications/gitlab-runner/values.yaml.gotmpl runner gitlab/gitlab-runner
Release "runner" has been upgraded. Happy Helming!
NAME: runner
LAST DEPLOYED: Sat Nov 16 08:05:03 2024
NAMESPACE: gitlab-managed-apps
STATUS: deployed
REVISION: 8
TEST SUITE: None
NOTES:
Your GitLab Runner should now be registered against the GitLab instance reachable at: "https://gitlab.com"
```

Verify the `runner-gitlab-*` pods upgrade and successfully register against gitlab.com via the logs!

## Hints

- We use the Secure token to install and spin up klusters!
  -- this means that you need to update Vault `k3s/server_token` secret with the value from server file `/var/lib/rancher/k3s/server/token`
  -- or else, no agents (specified by overriding the TARGET_HOST env var in the pipeline) will be able to connect!
- Pods are physically running here `root@tuxedolinuxrex:/run/k3s/containerd/io.containerd.runtime.v2.task/k8s.io/`
- Pod logs are found under the system path `/var/log/pods/*`
- On a complete new install (e.g. after nuking existing kluster), take care to delete all the referring, local `~/.kube/config` (incl. gitlab-runner machine!)
- There are many hardcoded versions in `.gitlab-ci.yml` -> ensure these are uptodate!

## TODOs

- On a fresh (re)install
  -- comment out the KUBE_CONTEXT variable in `.gitlab-ci.yml`
  -- delete the old k8s agent from https://gitlab.com/ackersonde/k3s-pifarm/-/clusters (since the cluster doesn't exist)
