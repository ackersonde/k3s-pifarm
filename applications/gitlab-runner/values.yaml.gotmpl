## REQUIRED VALUES
gitlabUrl: https://gitlab.com # when executing upgrade, hardcode this to https://gitlab.com while remotely hosted
runnerToken: ref+vault://github/ackersonde/gitlab#/k8s-runner_auth_token
replicas: 2

## Configure the maximum number of concurrent jobs
## - Documentation: https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-global-section
## - Default value: 10
## - Currently don't support auto-scaling.
concurrent: 4

## Defines in seconds how often to check GitLab for a new builds
## - Documentation: https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-global-section
## - Default value: 3
checkInterval: 3

imagePullSecrets:
  - name: gitlab-registry-credentials
  - name: docker-hub-secret

serviceAccount:
  create: true

## For RBAC support
rbac:
  create: true
  clusterWideAccess: true
  rules:
  - apiGroups: [""]
    resources: ["pods"]
    verbs: ["list", "get", "watch", "create", "delete"]
  - apiGroups: [""]
    resources: ["pods/exec"]
    verbs: ["get", "create"]
  - apiGroups: [""]
  - apiGroups: [""]
    resources: ["secrets", "configmaps", "pods/attach"]
    verbs: ["get", "list", "create", "update", "delete"]
  - apiGroups: [""]
    resources: ["services"]
    verbs: ["create"]

## Configuration for the Pods that the runner launches for each new job
runners:
  config: |
    [[runners]]
      executor="kubernetes"

      # "error: could not lock config file //.gitconfig: Permission denied". Wonder if setting builds_dir will help?
      # See <https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3511>
      builds_dir="/builds"
      environment=["HOME=/builds"]
      # the above also conflicts w/ the HELM_PLUGINS_DIR var as the docker image installs these in `/builds`
      # which is (probably) remounted by gitlab-runner and therefore no PLUGINS are available in the container :/

      [runners.kubernetes]
        node_selector_overwrite_allowed = ".*"
        helper_image = "bitnami/gitlab-runner-helper"
        privileged = false
        namespace = "gitlab-managed-apps"
        serviceaccountname = "gitlab-managed-apps"
        image = "registry.gitlab.com/ackersonde/base_build_deploy:latest"
        image_pull_secrets = ["gitlab-registry-credentials", "docker-hub-secret"]

  ## Specify the tags associated with the runner. Comma-separated list of tags.
  ## - Documentation: https://docs.gitlab.com/ce/ci/runners/#using-tags
  tags: kubernetes,cluster,k8s,k3s

  ## Determine whether the runner should also run jobs without tags.
  ## - Documentation: https://docs.gitlab.com/ee/ci/runners/configure_runners.html#set-a-runner-to-run-untagged-jobs
  runUntagged: true

  ## Run all containers with the privileged flag enabled
  ## This will allow the docker:dind image to run if you need to run Docker
  ## commands. Please read the docs before turning this on:
  ## - Documentation: https://docs.gitlab.com/runner/executors/kubernetes.html#using-docker-dind
  privileged: false

  ## Kubernetes related options to control which nodes executors use
  ## - Documentation: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/
  # nodeSelector:
  #   myLabel: myValue
  #
  ## Documentation: https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/
  # nodeTolerations:
  #   - key: myTaint
  #     operator: Equal
  #     value: myValue
  #     effect: NoSchedule

  ## If you can't find a setting you think should be here this may help:
  ## https://gitlab.com/gitlab-org/charts/gitlab-runner/-/blob/main/values.yaml
  ##
  ## The gitlab-runner chart uses `templates/configmap.yaml` to configure runners
  ## `configmap.yaml`'s `data.register-the-runner` transforms this file into runner CLI options
  ## `configmap.yaml`'s `data.config.toml` and `data.config.template.toml` transform this file into the runner's config.toml
  ##
  ## - Source code for `configmap.yaml` https://gitlab.com/gitlab-org/charts/gitlab-runner/-/blob/main/templates/configmap.yaml
  ## - Documentation for `config.toml` https://docs.gitlab.com/runner/executors/kubernetes.html#the-available-configtoml-settings
  ## - Source code for runner CLI options (see `KubernetesConfig` struct) https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/common/config.go

resources: {}
