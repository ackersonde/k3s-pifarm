service:
  ipFamilyPolicy: PreferDualStack
  spec:
    externalTrafficPolicy: Local  # left half of proxy setup for capturing real IP addresses

# Gateway API requires Traefik 3.1.0 or later
providers:
  kubernetesIngress:
    enabled: false
  kubernetesGateway:
    enabled: true

gateway:
  namespacePolicy: All
  listeners:
    web:
      protocol: HTTP
      port: 8000  # points to the default traefik web port ./default-traefik.yml#L617
    websecure:
      protocol: HTTPS
      port: 8443  # points to the default traefik websecure port ./default-traefik.yml#L656
      mode: Terminate
      certificateRefs:
        - name: letsencrypt-cert
          namespace: cert-manager

nodeSelector:
  node-role.kubernetes.io/master: "true"

ports:                            # right half of proxy setup for capturing real IP addresses
  web:
    forwardedHeaders:
      trustedIPs: [127.0.0.1/32,10.42.0.0/16,2001:cafe:42:1::/56]
    proxyProtocol:
      trustedIPs: [127.0.0.1/32,10.42.0.0/16,2001:cafe:42:1::/56]
  websecure:
    forwardedHeaders:
      trustedIPs: [127.0.0.1/32,10.42.0.0/16,2001:cafe:42:1::/56]
    proxyProtocol:
      trustedIPs: [127.0.0.1/32,10.42.0.0/16,2001:cafe:42:1::/56]

ingressRoute:
  dashboard: # default traefik dashboard. `dashboard-traefik` is my custom IngressRoute
    enabled: false

# Run pod as same user/group as the NFS server in order to write logs there
podSecurityContext:
  # /!\ When setting fsGroup, Kubernetes will recursively change ownership and
  # permissions for the contents of each volume to match the fsGroup. This can
  # be an issue when storing sensitive content like TLS Certificates /!\
  # fsGroup: 65532
  # -- Specifies the policy for changing ownership and permissions of volume contents to match the fsGroup.
  fsGroupChangePolicy: "OnRootMismatch"
  # -- these must be integers!
  runAsUser: 65534  # nobody
  runAsGroup: 65534 # nogroup
  runAsNonRoot: true

persistence:
  enabled: true
  name: traefik-nfs-logs
  existingClaim: pvc-nfs-dynamic
  accessMode: ReadWriteOnce
  size: 128Mi
  storageClass: nfs-csi
  path: /var/log/traefik

logs:
  general:
    # format: json # -- Alternative logging formats are json, logfmt, and common.
    # -- Alternative logging levels are DEBUG, PANIC, FATAL, ERROR, WARN, and INFO.
    level: INFO
  access:
    addInternals: true # this adds requests to Traefik's internal services (incl Dashboard) to the access logs (OMFG!)
    enabled: true
    format: common # -- Alternative logging formats are common, json, and combined.
    filePath: "/var/log/traefik/traefik-access.log"   # since mountpoint is "/var/log/traefik" above, we write access.log there
    ## To write the logs in an asynchronous fashion, specify a bufferingSize option.
    ## This option represents the number of log lines Traefik will keep in memory before writing
    ## them to the selected output. In some cases, this option can greatly help performances.
    # bufferingSize: 10
    fields:
      headers:
        defaultmode: keep
        names:
          Accept: drop
          Connection: drop
          Authorization: redact

